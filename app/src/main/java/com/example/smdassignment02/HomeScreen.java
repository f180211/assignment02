package com.example.smdassignment02;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.snackbar.Snackbar;


public class HomeScreen extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home_screen);
        this.getSupportActionBar().hide();

        Toast.makeText(HomeScreen.this, "Welcome to fast", Toast.LENGTH_SHORT).show();



    }

    public void quit(View view) {
        finish();
    }
}